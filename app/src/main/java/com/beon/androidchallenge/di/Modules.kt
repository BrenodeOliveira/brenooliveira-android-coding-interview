package com.beon.androidchallenge.di

import com.beon.androidchallenge.data.network.Api
import com.beon.androidchallenge.data.network.FactsClient
import com.beon.androidchallenge.data.network.HttpClient
import com.beon.androidchallenge.data.repository.FactLocalDataSource
import com.beon.androidchallenge.data.repository.FactRemoteDataSource
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
    factory {
        HttpClient(retrofit = Api.ApiService.initRetrofit())
    }

    factory {
        get<HttpClient>().create(
            service = FactsClient::class.java
        )
    }

    factory {
        FactRepository(FactRemoteDataSource(get()), FactLocalDataSource())
    }
}

val presentationModule = module {
    viewModel {
        MainViewModel(factRepository = get())
    }
}