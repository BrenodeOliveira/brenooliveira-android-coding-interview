package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.domain.model.Fact

class FactRepository(
    private var factRemoteDataSource: FactRemoteDataSource,
    private var factLocalDataSource: FactLocalDataSource
) {

    fun getFactForNumber(number: String): Fact {
        return factRemoteDataSource.getFactForNumber(number)
    }
}