package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.data.network.Api
import com.beon.androidchallenge.data.network.FactsClient
import com.beon.androidchallenge.domain.model.Fact
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FactRemoteDataSource(private val factsClient: FactsClient) {

    fun getFactForNumber(number: String): Fact {
        return factsClient.getFactForNumber(number)
    }

}