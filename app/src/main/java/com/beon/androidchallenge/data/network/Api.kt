package com.beon.androidchallenge.data.network

import com.beon.androidchallenge.domain.model.Fact
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private var API_BASE_URL = "http://numbersapi.com/"

class Api {

    object ApiService {
        fun initRetrofit(): Retrofit {
            return Retrofit.Builder().baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build()
        }
    }
}