package com.beon.androidchallenge

import android.app.Application
import com.beon.androidchallenge.di.dataModule
import com.beon.androidchallenge.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(applicationContext)
            modules(dataModule, presentationModule)
        }
    }
}