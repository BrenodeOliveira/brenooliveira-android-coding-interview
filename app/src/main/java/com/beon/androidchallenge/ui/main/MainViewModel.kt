package com.beon.androidchallenge.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel(
    private val factRepository: FactRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private val _currentFact: MutableLiveData<Fact?> = MutableLiveData()
    val currentFact: LiveData<Fact?> = _currentFact

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            _currentFact.postValue(null)
            return
        }

        viewModelScope.launch(dispatcher) {
            delay(500L)

            if (factRepository.getFactForNumber(number).text.isNullOrEmpty()) {
                _currentFact.postValue(null)
            } else {
                _currentFact.postValue(factRepository.getFactForNumber(number))
            }
        }
    }
}