package com.beon.androidchallenge.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beon.androidchallenge.R
import com.beon.androidchallenge.databinding.MainFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()
    private val binding: MainFragmentBinding by lazy { MainFragmentBinding.inflate(layoutInflater) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initObservers()
    }

    private fun initViews() {
        binding.run {
            numberEditText.addTextChangedListener {
                viewModel.searchNumberFact(it.toString())
            }

            numberEditText.setOnEditorActionListener { textView, actionId, keyEvent ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    viewModel.searchNumberFact(textView.text.toString())
                    return@setOnEditorActionListener true
                } else {
                    return@setOnEditorActionListener false
                }
            }

            retryButton.setOnClickListener {
                viewModel.searchNumberFact(numberEditText.text.toString())
            }

            numberEditText.requestFocus()
        }
    }

    private fun initObservers() {
        viewModel.currentFact.observeForever {
            if(it == null) {
                Toast.makeText(activity, "Internet error", Toast.LENGTH_SHORT).show()
            }

            if (binding.numberEditText.text.toString().isEmpty()) {
                binding.factTextView.setText(R.string.instructions)
            } else {
                binding.factTextView.text = it?.text
            }
        }
    }

}